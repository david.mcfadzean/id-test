import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [message, setMessage] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const getTest = await axios.get('/api/v1/test');
      setMessage(getTest.data.message);
    };

    fetchData();
  }, []);

  return (
    <div>
      <h1>Test</h1>
      <p>== {message} ==</p>
    </div>
  );
}

export default App;
